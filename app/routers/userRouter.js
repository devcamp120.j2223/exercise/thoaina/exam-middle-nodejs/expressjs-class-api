const express = require("express");

// import file
const userMiddleware = require("../middlewares/userMiddleware");
const listUser = require("../../data");

const userRouter = express.Router();

userRouter.use(userMiddleware);

userRouter.get("/users", (req, res) => {
    let age = req.query.age;
    if (!age) {
        res.status(200).json({
            listUser
        });
    }
    else {
        const newArr = new Array();
        for (var bIndex = 0; bIndex < listUser.length; bIndex++) {
            if (listUser[bIndex].age > Number(age)) {
                newArr.push(listUser[bIndex]);
            }
        }
        // console.log(newArr)
        res.status(200).json({
            newArr
        });
    }
});

userRouter.get("/users/:userId", (req, res) => {
    let id = req.params.userId;
    if (isNaN(id)) {
        res.status(400).json({
            message: `Internal server error: 400 Bad Request`,
        });
    }
    else {
        for (var bIndex = 0; bIndex < listUser.length; bIndex++) {
            if (listUser[bIndex].id == id) {
                res.status(200).json({
                    ...listUser[bIndex]
                });
            }
        }
    }
});

module.exports = userRouter;