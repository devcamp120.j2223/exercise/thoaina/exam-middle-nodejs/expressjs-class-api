// Khai báo class
class User {
    constructor(id, name, position, office, age, startDate) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.office = office;
        this.age = age;
        this.startDate = startDate;
    }
}

// Tạo mảng chứa các user
const listUser = new Array();
listUser.push(new User(1, "Airi Satou", "Accountant", "Tokyo", 33, "2008/11/28"));
listUser.push(new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", 47, "2009/10/09"));
listUser.push(new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", 66, "2009/01/12"));
listUser.push(new User(4, "Bradley Greer", "Software Engineer", "London", 41, "2009/01/12"));
listUser.push(new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", 28, "2011/06/07"));
listUser.push(new User(6, "Brielle Williamson", "Integration Specialist", "New York", 61, "2012/12/02"));
listUser.push(new User(7, "Bruno Nash", "Software Engineer", "London", 38, "2011/05/03"));
listUser.push(new User(8, "Caesar Vance", "Pre-Sales Support", "New York", 21, "2011/12/12"));
listUser.push(new User(9, "Cara Stevens", "Sales Assistant", "New York", 46, "2011/12/06"));
listUser.push(new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", 22, "2012/03/29"));

// export list user
module.exports = listUser;