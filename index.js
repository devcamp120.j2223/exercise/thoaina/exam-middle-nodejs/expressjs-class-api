// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

const userRouter = require("./app/routers/userRouter");

// Khởi tạo app express
const app = express();

// Khai báo cổng của project
const port = 8000;

// import middeware
app.use("/", userRouter);

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
});